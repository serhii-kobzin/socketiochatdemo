//
//  Message.swift
//  SocketIOChatDemo
//
//  Created by Serhii Kobzin on 5/4/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class Message {
    
    var senderNickname: String
    var text: String
    var sendTime: String
    
    init(senderNickname: String, text: String, sendTime: String) {
        self.senderNickname = senderNickname
        self.text = text
        self.sendTime = sendTime
    }
    
}
