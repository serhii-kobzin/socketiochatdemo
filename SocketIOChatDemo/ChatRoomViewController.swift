//
//  ChatRoomViewController.swift
//  SocketIOChatDemo
//
//  Created by Serhii Kobzin on 5/3/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class ChatRoomViewController: UIViewController {
    
    @IBOutlet weak var messagesTextView: UITextView!
    @IBOutlet weak var messageTextField: UITextField!
    
    private let manager = SocketIOManager.shared
    
    var nickname = ""
    var usersList = [User]()
    var messages = [Message]() {
        didSet {
            var commonText = ""
            for message in messages {
                commonText += ("[" + message.sendTime + "] " + message.senderNickname + " -> " + message.text + "\n")
            }
            messagesTextView.text = commonText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.setIncomingMessageHandler(completionHandler: { [weak self] message in
            guard let message = message else {
                return
            }
            self?.messages.append(message)
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        manager.exitChat(nickname: nickname, completionHandler: { })
    }
    
    @IBAction func sendButtonTouchUpInside(_ sender: UIButton) {
        guard let text = messageTextField.text, !text.isEmpty else {
            return
        }
        manager.sendMessage(text: text, nickname: nickname)
        messageTextField.text = ""
    }
}
