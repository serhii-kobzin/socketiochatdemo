//
//  HelloViewController.swift
//  SocketIOChatDemo
//
//  Created by Serhii Kobzin on 4/27/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import SocketIO
import UIKit


class HelloViewController: UIViewController {
    
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var beginButton: UIButton!
    
    let manager = SocketIOManager.shared
    
    @IBAction func beginButtonTouchUpInside(_ sender: UIButton) {
        guard let nickname = nicknameTextField.text, !nickname.isEmpty else {
            return
        }
        manager.enterChat(nickname: nickname, completionHandler: { [weak self] usersList in
            guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatRoomViewController") as? ChatRoomViewController else {
                return
            }
            viewController.nickname = nickname
            viewController.usersList = usersList
            self?.nicknameTextField.text = ""
            self?.navigationController?.pushViewController(viewController, animated: true)
        })
    }
    
}
