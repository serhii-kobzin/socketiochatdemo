//
//  User.swift
//  SocketIOChatDemo
//
//  Created by Serhii Kobzin on 5/3/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class User {
    
    var id: String
    var nickname: String
    var isConnected: Bool
    
    init(id: String, nickname: String, isConnected: Bool) {
        self.id = id
        self.nickname = nickname
        self.isConnected = isConnected
    }
    
}
