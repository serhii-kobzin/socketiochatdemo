//
//  SocketIOManager.swift
//  SocketIOChatDemo
//
//  Created by Serhii Kobzin on 5/2/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import SocketIO


class SocketIOManager {
    
    static let shared = SocketIOManager()
    
    private var manager: SocketManager
    private var socket: SocketIOClient
    
    init() {
        manager = SocketManager(socketURL: URL(string: "http://localhost:3000")!, config: [.log(true), .compress])
        socket = manager.defaultSocket
    }
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func enterChat(nickname: String, completionHandler: @escaping (_ usersList: [User]) -> ()) {
        socket.emit("connectUser", nickname)
        subscribeOnUpdateUsersListEvent(completionHandler: completionHandler)
        subscribeOnOtherEvents()
    }
    
    func exitChat(nickname: String, completionHandler: () -> ()) {
        socket.emit("exitUser", nickname)
        unsubscribeFromAllEvents()
        completionHandler()
    }
    
    func sendMessage(text: String, nickname: String) {
        socket.emit("chatMessage", nickname, text)
    }
    
    func setIncomingMessageHandler(completionHandler: @escaping (_ message: Message?) -> ()) {
        subscibeOnIncomingMessageEvent(completionHandler: completionHandler)
    }
    
    func sendStartTyping(nickname: String) {
        socket.emit("startType", nickname)
    }
    
    func sendStopTyping(nickname: String) {
        socket.emit("stopType", nickname)
    }
    
    private func subscribeOnUpdateUsersListEvent(completionHandler: @escaping (_ usersList: [User]) -> ()) {
        socket.on("userList") { data, ack in
            var usersList = [User]()
            guard let usersListArray = data.first as? [[String : Any]] else {
                completionHandler(usersList)
                return
            }
            for user in usersListArray {
                guard let id = user["id"] as? String, let nickname = user["nickname"] as? String, let isConnected = user["isConnected"] as? Bool else {
                    continue
                }
                let user = User(id: id, nickname: nickname, isConnected: isConnected)
                usersList.append(user)
            }
            completionHandler(usersList)
        }
    }
    
    private func subscibeOnIncomingMessageEvent(completionHandler: @escaping (_ message: Message?) -> ()) {
        socket.on("newChatMessage") { data, ack in
            guard let senderNickname = data[0] as? String, let text = data[1] as? String, let sendTime = data[2] as? String else {
                completionHandler(nil)
                return
            }
            let message = Message(senderNickname: senderNickname, text: text, sendTime: sendTime)
            completionHandler(message)
        }
    }
    
    private func subscribeOnOtherEvents() {
        socket.on("userConnectUpdate") { data, ack in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: data[0] as! [String: Any])
        }
        
        socket.on("userExitUpdate") { data, ack in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: data[0] as! String)
        }
        
        socket.on("userTypingUpdate") { data, ack in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: data[0] as? [String: Any])
        }
    }
    
    private func unsubscribeFromAllEvents() {
        socket.off("newChatMessage")
        socket.off("userConnectUpdate")
        socket.off("userExitUpdate")
        socket.off("userTypingUpdate")
        socket.off("userList")
    }
    
}
